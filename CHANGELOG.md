# Changelog

## [0.2.15] - 2020-04-27

### Added

- Add Delete Enrollment method in Face service

## [0.2.14] - 2020-04-22

### Added

- Fix FaceMask API

## [0.2.13] - 2020-04-10

### Added

- FaceMask API

## [0.2.12] - 2020-04-08

### Added

- Add FaceRecognitionOptions

## [0.2.11] - 2020-04-01

### Added

- Fix gRPC build on go

## [0.2.10] - 2020-03-30

### Added

- FacePose API
- PersonDetection API

## [0.2.9] - 2020-03-03

### Added

- Face matching boolean based on similarity threshold
- Add product detection support

## [0.2.8] - 2020-01-21

### Added

- Variation field on face recognition
- Variation field on face enrollment

## [0.2.7] - 2019-12-11

### Added

- FaceMatch API
- FaceMatchWithID API
- Shelf analytics (OSA, SOS and Realogram)
- Product type
- Add event metadata to hold context

### Modified

- Change timestamp of event to metadata
- Removed message from event metadata

## [0.2.7-alpha1] - 2019-08-26

### Added

- Added event to handling event from Nodeflux FIRE API

## [0.2.6] - 2019-07-19

### Added

- Face Enrollment analytics and API

## [0.2.5] - 2019-06-13

### Added

- FaceMatch API
- ImageClassification Analytic
- Add options in analytic.proto

## [0.2.4] - 2019-05-06

## [0.2.3] - 2019-04-29

### Added

- `auto_enrolled` flag in `FaceRecognition`. It is used to tell if the face id is automatically enrolled in the last search.

## [0.2.2] - 2019-04-26

### Added

- gRPC Gateway is used to generate REST API server from the gRPC service definition.

### Modified

- Use fixed `age` instead of `age_rage` in Face Demography response.

## [0.2.1] - 2019-03-08

### Added

- Add `thumbnails` field in face detection, vehicle detection, and license plate recognition
- Add `googleapis-common-protos` as python dependency

## [0.2.1] - 2019-03-07

### Added

- `metadata` field is added to `ImageAnalyticRequest`
- Makefile to compile protobuf to C++, Python and Go
- Auto generated docs (`make docs`)
- Linter (`make lint`)
- Gitlab CI support
- Apache 2.0 License File
- Python package installation script

### Modified

- All `v1` packages are downgraded to `v1beta1`
- `nodeflux.fire.v1` are moved to `nodeflux.api.v1beta1`
- `image` field in `ImageAnalyticRequest` are change to `nodeflux.types.v1beta1.Image`
- `BoundingBox` is moved to `nodeflux.types.v1beta1.BoundingBox` (used by `FaceDetection`, `VehicleDetection`, `LicensePlateRecognition`)

### Removed

- `VideoAnalytic` service are removed due to no usage. It's might be added back later with more complete API
- `FaceEnrollment` analytics is removed

## [0.1.2] - 2019-02-13

### Added

- Vehicle Detection API
- License Plate Recognition API
- Face Demography API

### Removed

- Age Detection API
- Gender Detection API

## [0.1.1] - 2019-01-03

### Modified

- Move to gitlab

## [0.1.0] - 2018-12-07

- gRPC ImageAnalytic service
- Face Detection API
- Face Recognition API
- Age Detection API
- Gender Detection API
- Extension API
