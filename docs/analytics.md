# Protocol Documentation
<a name="top"></a>

## Table of Contents

- [nodeflux/analytics/v1beta1/face_detection.proto](#nodeflux/analytics/v1beta1/face_detection.proto)
    - [FaceDetection](#nodeflux.analytics.v1beta1.FaceDetection)
  
  
  
  

- [nodeflux/analytics/v1beta1/face_match.proto](#nodeflux/analytics/v1beta1/face_match.proto)
    - [FaceMatch](#nodeflux.analytics.v1beta1.FaceMatch)
  
  
  
  

- [nodeflux/analytics/v1beta1/license_plate_recognition.proto](#nodeflux/analytics/v1beta1/license_plate_recognition.proto)
    - [LicensePlateRecognition](#nodeflux.analytics.v1beta1.LicensePlateRecognition)
  
  
  
  

- [nodeflux/analytics/v1beta1/vehicle_detection.proto](#nodeflux/analytics/v1beta1/vehicle_detection.proto)
    - [VehicleDetection](#nodeflux.analytics.v1beta1.VehicleDetection)
  
    - [VehicleDetection.VehicleType](#nodeflux.analytics.v1beta1.VehicleDetection.VehicleType)
  
  
  

- [nodeflux/analytics/v1beta1/face_enrollment.proto](#nodeflux/analytics/v1beta1/face_enrollment.proto)
    - [FaceEnrollment](#nodeflux.analytics.v1beta1.FaceEnrollment)
    - [FaceEnrollmentOptions](#nodeflux.analytics.v1beta1.FaceEnrollmentOptions)
  
  
  
  

- [nodeflux/analytics/v1beta1/face_demography.proto](#nodeflux/analytics/v1beta1/face_demography.proto)
    - [FaceDemography](#nodeflux.analytics.v1beta1.FaceDemography)
  
    - [FaceDemography.Gender](#nodeflux.analytics.v1beta1.FaceDemography.Gender)
  
  
  

- [nodeflux/analytics/v1beta1/share_on_shelf.proto](#nodeflux/analytics/v1beta1/share_on_shelf.proto)
    - [ShareOnShelf](#nodeflux.analytics.v1beta1.ShareOnShelf)
  
  
  
  

- [nodeflux/analytics/v1beta1/image_classification.proto](#nodeflux/analytics/v1beta1/image_classification.proto)
    - [ImageClassification](#nodeflux.analytics.v1beta1.ImageClassification)
  
  
  
  

- [nodeflux/analytics/v1beta1/analytic.proto](#nodeflux/analytics/v1beta1/analytic.proto)
    - [Analytic](#nodeflux.analytics.v1beta1.Analytic)
  
    - [Analytic.Type](#nodeflux.analytics.v1beta1.Analytic.Type)
  
  
  

- [nodeflux/analytics/v1beta1/on_shelf_availability.proto](#nodeflux/analytics/v1beta1/on_shelf_availability.proto)
    - [OnShelfAvailability](#nodeflux.analytics.v1beta1.OnShelfAvailability)
  
  
  
  

- [nodeflux/analytics/v1beta1/shelf_standard.proto](#nodeflux/analytics/v1beta1/shelf_standard.proto)
    - [ShelfStandard](#nodeflux.analytics.v1beta1.ShelfStandard)
  
  
  
  

- [nodeflux/analytics/v1beta1/product_detection.proto](#nodeflux/analytics/v1beta1/product_detection.proto)
    - [ProductDetection](#nodeflux.analytics.v1beta1.ProductDetection)
  
  
  
  

- [nodeflux/analytics/v1beta1/face_recognition.proto](#nodeflux/analytics/v1beta1/face_recognition.proto)
    - [FaceCandidate](#nodeflux.analytics.v1beta1.FaceCandidate)
    - [FaceRecognition](#nodeflux.analytics.v1beta1.FaceRecognition)
  
  
  
  

- [Scalar Value Types](#scalar-value-types)



<a name="nodeflux/analytics/v1beta1/face_detection.proto"></a>
<p align="right"><a href="#top">Top</a></p>

## nodeflux/analytics/v1beta1/face_detection.proto



<a name="nodeflux.analytics.v1beta1.FaceDetection"></a>

### FaceDetection
Face detection analytic result.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| bounding_box | [nodeflux.types.v1beta1.BoundingBox](#nodeflux.types.v1beta1.BoundingBox) |  | Bounding box around the detected face. |
| confidence | [float](#float) |  | Confidence of the face detection. |
| thumbnails | [nodeflux.types.v1beta1.Image](#nodeflux.types.v1beta1.Image) | repeated | Cropped images focused on the detected face. |
| landmarks | [nodeflux.types.v1beta1.Landmark](#nodeflux.types.v1beta1.Landmark) | repeated | Landmark points of the detected face |





 

 

 

 



<a name="nodeflux/analytics/v1beta1/face_match.proto"></a>
<p align="right"><a href="#top">Top</a></p>

## nodeflux/analytics/v1beta1/face_match.proto



<a name="nodeflux.analytics.v1beta1.FaceMatch"></a>

### FaceMatch
1:1 Face match analytic result.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| similarity | [float](#float) |  | Similarity score between compared embeddings. |
| match | [bool](#bool) |  | Face matching boolean based on similarity threshold. |





 

 

 

 



<a name="nodeflux/analytics/v1beta1/license_plate_recognition.proto"></a>
<p align="right"><a href="#top">Top</a></p>

## nodeflux/analytics/v1beta1/license_plate_recognition.proto



<a name="nodeflux.analytics.v1beta1.LicensePlateRecognition"></a>

### LicensePlateRecognition
License Plate detection analytic result.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| bounding_box | [nodeflux.types.v1beta1.BoundingBox](#nodeflux.types.v1beta1.BoundingBox) |  | Bounding box around the recognized license plate. |
| confidence | [float](#float) |  | Confidence of the license plate recognition. |
| license_plate_number | [string](#string) |  | Character in the recognized license plate. |
| thumbnail | [nodeflux.types.v1beta1.Image](#nodeflux.types.v1beta1.Image) |  | Cropped image focused on the license plate. |





 

 

 

 



<a name="nodeflux/analytics/v1beta1/vehicle_detection.proto"></a>
<p align="right"><a href="#top">Top</a></p>

## nodeflux/analytics/v1beta1/vehicle_detection.proto



<a name="nodeflux.analytics.v1beta1.VehicleDetection"></a>

### VehicleDetection
Vehicle detection analytic result.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| type | [VehicleDetection.VehicleType](#nodeflux.analytics.v1beta1.VehicleDetection.VehicleType) |  | Detected vehicle |
| bounding_box | [nodeflux.types.v1beta1.BoundingBox](#nodeflux.types.v1beta1.BoundingBox) |  | Bounding box around the detected vehicle. |
| confidence | [float](#float) |  | Confidence of the vehicle detection. |
| thumbnails | [nodeflux.types.v1beta1.Image](#nodeflux.types.v1beta1.Image) | repeated | Cropped images focused on the detected vehicle. |





 


<a name="nodeflux.analytics.v1beta1.VehicleDetection.VehicleType"></a>

### VehicleDetection.VehicleType
Vehicle types.

| Name | Number | Description |
| ---- | ------ | ----------- |
| UNKNOWN | 0 |  |
| CAR | 1 |  |
| BUS | 2 |  |
| TRUCK | 3 |  |
| MOTORCYCLE | 4 |  |


 

 

 



<a name="nodeflux/analytics/v1beta1/face_enrollment.proto"></a>
<p align="right"><a href="#top">Top</a></p>

## nodeflux/analytics/v1beta1/face_enrollment.proto



<a name="nodeflux.analytics.v1beta1.FaceEnrollment"></a>

### FaceEnrollment
Face Enrollment analytic result.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| face_id | [uint64](#uint64) |  | the face id after adding the embedding to database |
| variation | [uint64](#uint64) |  | the face variation id of a particular face |






<a name="nodeflux.analytics.v1beta1.FaceEnrollmentOptions"></a>

### FaceEnrollmentOptions
Face enrollment request options


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| face_id | [uint64](#uint64) |  |  |





 

 

 

 



<a name="nodeflux/analytics/v1beta1/face_demography.proto"></a>
<p align="right"><a href="#top">Top</a></p>

## nodeflux/analytics/v1beta1/face_demography.proto



<a name="nodeflux.analytics.v1beta1.FaceDemography"></a>

### FaceDemography



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| gender | [FaceDemography.Gender](#nodeflux.analytics.v1beta1.FaceDemography.Gender) |  | Detected gender. |
| gender_confidence | [float](#float) |  | Confidence of the gender detection. |
| age | [int32](#int32) |  | The estimated age. |





 


<a name="nodeflux.analytics.v1beta1.FaceDemography.Gender"></a>

### FaceDemography.Gender
Gender types.

| Name | Number | Description |
| ---- | ------ | ----------- |
| UNKNOWN | 0 |  |
| MALE | 1 |  |
| FEMALE | 2 |  |


 

 

 



<a name="nodeflux/analytics/v1beta1/share_on_shelf.proto"></a>
<p align="right"><a href="#top">Top</a></p>

## nodeflux/analytics/v1beta1/share_on_shelf.proto



<a name="nodeflux.analytics.v1beta1.ShareOnShelf"></a>

### ShareOnShelf
Share on Shelf analytic result.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| known_score | [float](#float) |  | Share on Shelf of known product score. |
| competitor_score | [float](#float) |  | Share on Shelf of competitor product score. |





 

 

 

 



<a name="nodeflux/analytics/v1beta1/image_classification.proto"></a>
<p align="right"><a href="#top">Top</a></p>

## nodeflux/analytics/v1beta1/image_classification.proto



<a name="nodeflux.analytics.v1beta1.ImageClassification"></a>

### ImageClassification
Image classification analytic result.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| label | [string](#string) |  | Class label of the image. |
| confidence | [float](#float) |  | Probablity score of the image. |





 

 

 

 



<a name="nodeflux/analytics/v1beta1/analytic.proto"></a>
<p align="right"><a href="#top">Top</a></p>

## nodeflux/analytics/v1beta1/analytic.proto



<a name="nodeflux.analytics.v1beta1.Analytic"></a>

### Analytic
The type of image or video analytic to be performed in
the Nodeflux FIRE API.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| type | [Analytic.Type](#nodeflux.analytics.v1beta1.Analytic.Type) |  | The analytic type to be performed. |
| extension | [string](#string) |  | Extension name. Only valid if type is EXTENSION. |
| options | [google.protobuf.Any](#google.protobuf.Any) |  | Analytic specific options. |





 


<a name="nodeflux.analytics.v1beta1.Analytic.Type"></a>

### Analytic.Type
Type of Nodeflux FIRE APIs.

| Name | Number | Description |
| ---- | ------ | ----------- |
| TYPE_UNSPECIFIED | 0 | Unspecified feature type. |
| EXTENSION | 1 | Reserved for types not defined in the public FIRE API. Used for custom analytics or prototypes. |
| FACE_DETECTION | 2 | Run face detection. |
| FACE_ENROLLMENT | 3 | Run face enrollment. |
| FACE_RECOGNITION | 4 | Run face recognition. |
| FACE_DEMOGRAPHY | 5 | Run face demography. |
| VEHICLE_DETECTION | 6 | Run vehicle detection. |
| LICENSE_PLATE_RECOGNITION | 7 | Run license plate recognition |
| FACE_MATCH | 8 | Run 1:1 face match. |
| PRODUCT_DETECTION | 9 | Run product detection. |
| ON_SHELF_AVAILABILITY | 10 | Run shelf availability. |
| SHARE_ON_SHELF | 11 | Run share on shelf. |
| SHELF_STANDARD | 12 | Run shelf standard. |


 

 

 



<a name="nodeflux/analytics/v1beta1/on_shelf_availability.proto"></a>
<p align="right"><a href="#top">Top</a></p>

## nodeflux/analytics/v1beta1/on_shelf_availability.proto



<a name="nodeflux.analytics.v1beta1.OnShelfAvailability"></a>

### OnShelfAvailability
On Shelf Availability analytic result.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| score | [float](#float) |  | On Shelf Availability score. |
| unavailable_products | [nodeflux.types.v1beta1.Product](#nodeflux.types.v1beta1.Product) | repeated | Unavailable product in realogram. |





 

 

 

 



<a name="nodeflux/analytics/v1beta1/shelf_standard.proto"></a>
<p align="right"><a href="#top">Top</a></p>

## nodeflux/analytics/v1beta1/shelf_standard.proto



<a name="nodeflux.analytics.v1beta1.ShelfStandard"></a>

### ShelfStandard
Shelf standard analytic result.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| score | [float](#float) |  | Score result. |





 

 

 

 



<a name="nodeflux/analytics/v1beta1/product_detection.proto"></a>
<p align="right"><a href="#top">Top</a></p>

## nodeflux/analytics/v1beta1/product_detection.proto



<a name="nodeflux.analytics.v1beta1.ProductDetection"></a>

### ProductDetection
Product Detection analytic result.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| image_visualized | [nodeflux.types.v1beta1.Image](#nodeflux.types.v1beta1.Image) |  | Detected visualized image. |
| products | [nodeflux.types.v1beta1.Product](#nodeflux.types.v1beta1.Product) | repeated | Repeated detected product. |





 

 

 

 



<a name="nodeflux/analytics/v1beta1/face_recognition.proto"></a>
<p align="right"><a href="#top">Top</a></p>

## nodeflux/analytics/v1beta1/face_recognition.proto



<a name="nodeflux.analytics.v1beta1.FaceCandidate"></a>

### FaceCandidate
Face candidate from the face recognition analytic.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| face_id | [uint64](#uint64) |  |  |
| confidence | [float](#float) |  | Confidence of the recognition. Deprecated: use `similarity` instead. |
| auto_enrolled | [bool](#bool) |  | If auto_enrolled is true, the face id is enrolled automatically by the current search request. |
| similarity | [float](#float) |  | Similarity score. |
| variation | [uint64](#uint64) |  | the face variation id of a particular face |






<a name="nodeflux.analytics.v1beta1.FaceRecognition"></a>

### FaceRecognition
Face recognition analytic result.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| candidates | [FaceCandidate](#nodeflux.analytics.v1beta1.FaceCandidate) | repeated | List of candidates that matches the requested face. If candidates is set, the face recognition analytic has been successful. |
| face_confidence | [float](#float) |  | Confidence of the face detection. |
| registered | [bool](#bool) |  | True if the candidates are found in the known `keyspace`. |





 

 

 

 



## Scalar Value Types

| .proto Type | Notes | C++ Type | Java Type | Python Type |
| ----------- | ----- | -------- | --------- | ----------- |
| <a name="double" /> double |  | double | double | float |
| <a name="float" /> float |  | float | float | float |
| <a name="int32" /> int32 | Uses variable-length encoding. Inefficient for encoding negative numbers – if your field is likely to have negative values, use sint32 instead. | int32 | int | int |
| <a name="int64" /> int64 | Uses variable-length encoding. Inefficient for encoding negative numbers – if your field is likely to have negative values, use sint64 instead. | int64 | long | int/long |
| <a name="uint32" /> uint32 | Uses variable-length encoding. | uint32 | int | int/long |
| <a name="uint64" /> uint64 | Uses variable-length encoding. | uint64 | long | int/long |
| <a name="sint32" /> sint32 | Uses variable-length encoding. Signed int value. These more efficiently encode negative numbers than regular int32s. | int32 | int | int |
| <a name="sint64" /> sint64 | Uses variable-length encoding. Signed int value. These more efficiently encode negative numbers than regular int64s. | int64 | long | int/long |
| <a name="fixed32" /> fixed32 | Always four bytes. More efficient than uint32 if values are often greater than 2^28. | uint32 | int | int |
| <a name="fixed64" /> fixed64 | Always eight bytes. More efficient than uint64 if values are often greater than 2^56. | uint64 | long | int/long |
| <a name="sfixed32" /> sfixed32 | Always four bytes. | int32 | int | int |
| <a name="sfixed64" /> sfixed64 | Always eight bytes. | int64 | long | int/long |
| <a name="bool" /> bool |  | bool | boolean | boolean |
| <a name="string" /> string | A string must always contain UTF-8 encoded or 7-bit ASCII text. | string | String | str/unicode |
| <a name="bytes" /> bytes | May contain any arbitrary sequence of bytes. | string | ByteString | str |

