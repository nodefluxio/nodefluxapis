# Protocol Documentation
<a name="top"></a>

## Table of Contents

- [nodeflux/api/v1beta1/image_analytic.proto](#nodeflux/api/v1beta1/image_analytic.proto)
    - [BatchImageAnalyticRequest](#nodeflux.api.v1beta1.BatchImageAnalyticRequest)
    - [BatchImageAnalyticResponse](#nodeflux.api.v1beta1.BatchImageAnalyticResponse)
    - [ImageAnalyticRequest](#nodeflux.api.v1beta1.ImageAnalyticRequest)
    - [ImageAnalyticResponse](#nodeflux.api.v1beta1.ImageAnalyticResponse)
  
  
  
    - [ImageAnalytic](#nodeflux.api.v1beta1.ImageAnalytic)
  

- [nodeflux/api/v1beta1/face.proto](#nodeflux/api/v1beta1/face.proto)
    - [FaceDemographyRequest](#nodeflux.api.v1beta1.FaceDemographyRequest)
    - [FaceDemographyResponse](#nodeflux.api.v1beta1.FaceDemographyResponse)
    - [FaceMatchRequest](#nodeflux.api.v1beta1.FaceMatchRequest)
    - [FaceMatchResponse](#nodeflux.api.v1beta1.FaceMatchResponse)
    - [FaceMatchWithIDRequest](#nodeflux.api.v1beta1.FaceMatchWithIDRequest)
    - [FaceRecognitionRequest](#nodeflux.api.v1beta1.FaceRecognitionRequest)
    - [FaceRecognitionResponse](#nodeflux.api.v1beta1.FaceRecognitionResponse)
  
  
  
    - [Face](#nodeflux.api.v1beta1.Face)
  

- [Scalar Value Types](#scalar-value-types)



<a name="nodeflux/api/v1beta1/image_analytic.proto"></a>
<p align="right"><a href="#top">Top</a></p>

## nodeflux/api/v1beta1/image_analytic.proto



<a name="nodeflux.api.v1beta1.BatchImageAnalyticRequest"></a>

### BatchImageAnalyticRequest
Multiple image analytics requests for a single service call.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| requests | [ImageAnalyticRequest](#nodeflux.api.v1beta1.ImageAnalyticRequest) | repeated | List of image analytic requests for this batch. |






<a name="nodeflux.api.v1beta1.BatchImageAnalyticResponse"></a>

### BatchImageAnalyticResponse
Response to a batch image analytic request.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| responses | [ImageAnalyticResponse](#nodeflux.api.v1beta1.ImageAnalyticResponse) | repeated | Individual responses to image analytics requests within the batch. |






<a name="nodeflux.api.v1beta1.ImageAnalyticRequest"></a>

### ImageAnalyticRequest
Request for performing one or more analytics over an image.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| image | [nodeflux.types.v1beta1.Image](#nodeflux.types.v1beta1.Image) |  | The image to be processed. |
| analytics | [nodeflux.analytics.v1beta1.Analytic](#nodeflux.analytics.v1beta1.Analytic) | repeated | Requested analytics to be performed. |
| metadata | [nodeflux.types.v1beta1.ImageMetadata](#nodeflux.types.v1beta1.ImageMetadata) |  | Metadata of the image. Could be used to add more context to the image. e.g. where the image comes from and when. |






<a name="nodeflux.api.v1beta1.ImageAnalyticResponse"></a>

### ImageAnalyticResponse
Response to an image analytic request.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| error | [google.rpc.Status](#google.rpc.Status) |  | If set, represents the error message for the operation. Note that filled-in image analytics response are guaranteed to be correct, even when `error` is set. |
| extensions | [google.protobuf.Any](#google.protobuf.Any) | repeated | If present, extension analytics has completed successfully. |
| face_detections | [nodeflux.analytics.v1beta1.FaceDetection](#nodeflux.analytics.v1beta1.FaceDetection) | repeated | If present, face detection has completed successfully. |
| face_recognitions | [nodeflux.analytics.v1beta1.FaceRecognition](#nodeflux.analytics.v1beta1.FaceRecognition) | repeated | If present, face recognition has completed successfully. |
| face_enrollments | [nodeflux.analytics.v1beta1.FaceEnrollment](#nodeflux.analytics.v1beta1.FaceEnrollment) | repeated | If present, face enrollment has completed successfully. |
| face_demographics | [nodeflux.analytics.v1beta1.FaceDemography](#nodeflux.analytics.v1beta1.FaceDemography) | repeated | If present, face demographics has completed successfully. |
| vehicle_detections | [nodeflux.analytics.v1beta1.VehicleDetection](#nodeflux.analytics.v1beta1.VehicleDetection) | repeated | If present, vehicle has completed successfully. |
| license_plate_recognitions | [nodeflux.analytics.v1beta1.LicensePlateRecognition](#nodeflux.analytics.v1beta1.LicensePlateRecognition) | repeated | If present, license plate recognition has completed successfully. |
| product_detection | [nodeflux.analytics.v1beta1.ProductDetection](#nodeflux.analytics.v1beta1.ProductDetection) |  | If present, product detection has completed successfully. |
| on_shelf_availability | [nodeflux.analytics.v1beta1.OnShelfAvailability](#nodeflux.analytics.v1beta1.OnShelfAvailability) |  | If present, on shelf availability has completed successfully. |
| share_on_shelf | [nodeflux.analytics.v1beta1.ShareOnShelf](#nodeflux.analytics.v1beta1.ShareOnShelf) |  | If present, on shelf on shelf has completed successfully. |
| shelf_standard | [nodeflux.analytics.v1beta1.ShelfStandard](#nodeflux.analytics.v1beta1.ShelfStandard) |  | If present, on shelf on shelf has completed successfully. |
| classification | [nodeflux.analytics.v1beta1.ImageClassification](#nodeflux.analytics.v1beta1.ImageClassification) |  |  |





 

 

 


<a name="nodeflux.api.v1beta1.ImageAnalytic"></a>

### ImageAnalytic
The ImageAnalytic service performs Nodeflux FIRE analytics over
client images. Supports synchronous and asynchronous service.

| Method Name | Request Type | Response Type | Description |
| ----------- | ------------ | ------------- | ------------|
| BatchImageAnalytic | [BatchImageAnalyticRequest](#nodeflux.api.v1beta1.BatchImageAnalyticRequest) | [BatchImageAnalyticResponse](#nodeflux.api.v1beta1.BatchImageAnalyticResponse) | Run image analytics for a batch of images |
| StreamImageAnalytic | [ImageAnalyticRequest](#nodeflux.api.v1beta1.ImageAnalyticRequest) stream | [ImageAnalyticResponse](#nodeflux.api.v1beta1.ImageAnalyticResponse) stream | Run image analytics for a stream of images |

 



<a name="nodeflux/api/v1beta1/face.proto"></a>
<p align="right"><a href="#top">Top</a></p>

## nodeflux/api/v1beta1/face.proto



<a name="nodeflux.api.v1beta1.FaceDemographyRequest"></a>

### FaceDemographyRequest



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| image | [nodeflux.types.v1beta1.Image](#nodeflux.types.v1beta1.Image) |  |  |
| metadata | [nodeflux.types.v1beta1.ImageMetadata](#nodeflux.types.v1beta1.ImageMetadata) |  |  |






<a name="nodeflux.api.v1beta1.FaceDemographyResponse"></a>

### FaceDemographyResponse



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| face_demographics | [nodeflux.analytics.v1beta1.FaceDemography](#nodeflux.analytics.v1beta1.FaceDemography) | repeated |  |






<a name="nodeflux.api.v1beta1.FaceMatchRequest"></a>

### FaceMatchRequest



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| image_a | [nodeflux.types.v1beta1.Image](#nodeflux.types.v1beta1.Image) |  |  |
| image_b | [nodeflux.types.v1beta1.Image](#nodeflux.types.v1beta1.Image) |  |  |
| metadata | [nodeflux.types.v1beta1.ImageMetadata](#nodeflux.types.v1beta1.ImageMetadata) |  |  |






<a name="nodeflux.api.v1beta1.FaceMatchResponse"></a>

### FaceMatchResponse



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| face_match | [nodeflux.analytics.v1beta1.FaceMatch](#nodeflux.analytics.v1beta1.FaceMatch) |  |  |






<a name="nodeflux.api.v1beta1.FaceMatchWithIDRequest"></a>

### FaceMatchWithIDRequest



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| image | [nodeflux.types.v1beta1.Image](#nodeflux.types.v1beta1.Image) |  |  |
| id | [uint64](#uint64) |  |  |
| metadata | [nodeflux.types.v1beta1.ImageMetadata](#nodeflux.types.v1beta1.ImageMetadata) |  |  |






<a name="nodeflux.api.v1beta1.FaceRecognitionRequest"></a>

### FaceRecognitionRequest



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| image | [nodeflux.types.v1beta1.Image](#nodeflux.types.v1beta1.Image) |  |  |
| metadata | [nodeflux.types.v1beta1.ImageMetadata](#nodeflux.types.v1beta1.ImageMetadata) |  |  |






<a name="nodeflux.api.v1beta1.FaceRecognitionResponse"></a>

### FaceRecognitionResponse



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| face_recognitions | [nodeflux.analytics.v1beta1.FaceRecognition](#nodeflux.analytics.v1beta1.FaceRecognition) | repeated |  |





 

 

 


<a name="nodeflux.api.v1beta1.Face"></a>

### Face


| Method Name | Request Type | Response Type | Description |
| ----------- | ------------ | ------------- | ------------|
| Demography | [FaceDemographyRequest](#nodeflux.api.v1beta1.FaceDemographyRequest) | [FaceDemographyResponse](#nodeflux.api.v1beta1.FaceDemographyResponse) |  |
| Recognition | [FaceRecognitionRequest](#nodeflux.api.v1beta1.FaceRecognitionRequest) | [FaceRecognitionResponse](#nodeflux.api.v1beta1.FaceRecognitionResponse) |  |
| Match | [FaceMatchRequest](#nodeflux.api.v1beta1.FaceMatchRequest) | [FaceMatchResponse](#nodeflux.api.v1beta1.FaceMatchResponse) |  |
| MatchWithID | [FaceMatchWithIDRequest](#nodeflux.api.v1beta1.FaceMatchWithIDRequest) | [FaceMatchResponse](#nodeflux.api.v1beta1.FaceMatchResponse) |  |

 



## Scalar Value Types

| .proto Type | Notes | C++ Type | Java Type | Python Type |
| ----------- | ----- | -------- | --------- | ----------- |
| <a name="double" /> double |  | double | double | float |
| <a name="float" /> float |  | float | float | float |
| <a name="int32" /> int32 | Uses variable-length encoding. Inefficient for encoding negative numbers – if your field is likely to have negative values, use sint32 instead. | int32 | int | int |
| <a name="int64" /> int64 | Uses variable-length encoding. Inefficient for encoding negative numbers – if your field is likely to have negative values, use sint64 instead. | int64 | long | int/long |
| <a name="uint32" /> uint32 | Uses variable-length encoding. | uint32 | int | int/long |
| <a name="uint64" /> uint64 | Uses variable-length encoding. | uint64 | long | int/long |
| <a name="sint32" /> sint32 | Uses variable-length encoding. Signed int value. These more efficiently encode negative numbers than regular int32s. | int32 | int | int |
| <a name="sint64" /> sint64 | Uses variable-length encoding. Signed int value. These more efficiently encode negative numbers than regular int64s. | int64 | long | int/long |
| <a name="fixed32" /> fixed32 | Always four bytes. More efficient than uint32 if values are often greater than 2^28. | uint32 | int | int |
| <a name="fixed64" /> fixed64 | Always eight bytes. More efficient than uint64 if values are often greater than 2^56. | uint64 | long | int/long |
| <a name="sfixed32" /> sfixed32 | Always four bytes. | int32 | int | int |
| <a name="sfixed64" /> sfixed64 | Always eight bytes. | int64 | long | int/long |
| <a name="bool" /> bool |  | bool | boolean | boolean |
| <a name="string" /> string | A string must always contain UTF-8 encoded or 7-bit ASCII text. | string | String | str/unicode |
| <a name="bytes" /> bytes | May contain any arbitrary sequence of bytes. | string | ByteString | str |

