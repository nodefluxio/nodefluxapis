# Protocol Documentation
<a name="top"></a>

## Table of Contents

- [nodeflux/types/v1beta1/landmark.proto](#nodeflux/types/v1beta1/landmark.proto)
    - [Landmark](#nodeflux.types.v1beta1.Landmark)
  
    - [Landmark.LandmarkType](#nodeflux.types.v1beta1.Landmark.LandmarkType)
  
  
  

- [nodeflux/types/v1beta1/image.proto](#nodeflux/types/v1beta1/image.proto)
    - [Image](#nodeflux.types.v1beta1.Image)
    - [ImageMetadata](#nodeflux.types.v1beta1.ImageMetadata)
    - [ImageMetadata.LabelsEntry](#nodeflux.types.v1beta1.ImageMetadata.LabelsEntry)
    - [ImageSize](#nodeflux.types.v1beta1.ImageSize)
  
    - [Image.Encoding](#nodeflux.types.v1beta1.Image.Encoding)
  
  
  

- [nodeflux/types/v1beta1/bounding_box.proto](#nodeflux/types/v1beta1/bounding_box.proto)
    - [BoundingBox](#nodeflux.types.v1beta1.BoundingBox)
  
  
  
  

- [nodeflux/types/v1beta1/embedding.proto](#nodeflux/types/v1beta1/embedding.proto)
    - [Embedding](#nodeflux.types.v1beta1.Embedding)
  
  
  
  

- [nodeflux/types/v1beta1/product.proto](#nodeflux/types/v1beta1/product.proto)
    - [Product](#nodeflux.types.v1beta1.Product)
  
  
  
  

- [Scalar Value Types](#scalar-value-types)



<a name="nodeflux/types/v1beta1/landmark.proto"></a>
<p align="right"><a href="#top">Top</a></p>

## nodeflux/types/v1beta1/landmark.proto



<a name="nodeflux.types.v1beta1.Landmark"></a>

### Landmark



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| x | [float](#float) |  | x coordinate value |
| y | [float](#float) |  | y coordinate value |
| type | [Landmark.LandmarkType](#nodeflux.types.v1beta1.Landmark.LandmarkType) |  | The type of the Landmark |





 


<a name="nodeflux.types.v1beta1.Landmark.LandmarkType"></a>

### Landmark.LandmarkType
Types of the Landmark

| Name | Number | Description |
| ---- | ------ | ----------- |
| UNKNOWN | 0 |  |
| EYE_LEFT | 1 |  |
| EYE_RIGHT | 2 |  |
| NOSE | 3 |  |
| MOUTH_LEFT | 4 |  |
| MOUTH_RIGHT | 5 |  |


 

 

 



<a name="nodeflux/types/v1beta1/image.proto"></a>
<p align="right"><a href="#top">Top</a></p>

## nodeflux/types/v1beta1/image.proto



<a name="nodeflux.types.v1beta1.Image"></a>

### Image



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| encoding | [Image.Encoding](#nodeflux.types.v1beta1.Image.Encoding) |  | Image encoding type. |
| size | [ImageSize](#nodeflux.types.v1beta1.ImageSize) |  | Image size (height, width, and channel). This field is required if the `encoding` is set to `IMAGE_ENCODING_RGB24`. |
| address | [string](#string) |  | Address of image stored in a remote storage. The address must be a fully qualified URL. e.g: - http://some-image-source.com/path/to/image.jpg - s3://my-object-storage.com/path/to/image.jpg |
| content | [bytes](#bytes) |  | Image content, represented as stream of bytes. If both `address` and `content` are provided, `content` takes precedence. |






<a name="nodeflux.types.v1beta1.ImageMetadata"></a>

### ImageMetadata



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| labels | [ImageMetadata.LabelsEntry](#nodeflux.types.v1beta1.ImageMetadata.LabelsEntry) | repeated |  |
| timestamp | [google.protobuf.Timestamp](#google.protobuf.Timestamp) |  |  |






<a name="nodeflux.types.v1beta1.ImageMetadata.LabelsEntry"></a>

### ImageMetadata.LabelsEntry



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| key | [string](#string) |  |  |
| value | [string](#string) |  |  |






<a name="nodeflux.types.v1beta1.ImageSize"></a>

### ImageSize
Image size descriptor


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| height | [int32](#int32) |  | Image height in pixels |
| width | [int32](#int32) |  | Image width in pixels |
| channel | [int32](#int32) |  | Image&#39;s number of channel |





 


<a name="nodeflux.types.v1beta1.Image.Encoding"></a>

### Image.Encoding
Image encoding types

| Name | Number | Description |
| ---- | ------ | ----------- |
| IMAGE_ENCODING_UNKNOWN | 0 | Unknwon image encoding. |
| IMAGE_ENCODING_RGB24 | 1 | Image encoded as rgb24 |
| IMAGE_ENCODING_JPEG | 2 | Image encoded as jpeg. |


 

 

 



<a name="nodeflux/types/v1beta1/bounding_box.proto"></a>
<p align="right"><a href="#top">Top</a></p>

## nodeflux/types/v1beta1/bounding_box.proto



<a name="nodeflux.types.v1beta1.BoundingBox"></a>

### BoundingBox
Bounding box around a detected object.
The coordinates are in ratio of overall image size.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| top | [float](#float) |  | Top (y) coordinate of the bounding box. |
| left | [float](#float) |  | Top (x) coordinate of the bounding box. |
| height | [float](#float) |  | Height of the bounding box. |
| width | [float](#float) |  | Width of the bounding box. |





 

 

 

 



<a name="nodeflux/types/v1beta1/embedding.proto"></a>
<p align="right"><a href="#top">Top</a></p>

## nodeflux/types/v1beta1/embedding.proto



<a name="nodeflux.types.v1beta1.Embedding"></a>

### Embedding
Vector representation of an image


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| data | [float](#float) | repeated |  |





 

 

 

 



<a name="nodeflux/types/v1beta1/product.proto"></a>
<p align="right"><a href="#top">Top</a></p>

## nodeflux/types/v1beta1/product.proto



<a name="nodeflux.types.v1beta1.Product"></a>

### Product
Single product in shelf analytic.


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| name | [string](#string) |  | Product name. |
| label | [uint64](#uint64) |  | Product label. |
| confidence | [float](#float) |  | Confidence score. |
| bounding_box | [BoundingBox](#nodeflux.types.v1beta1.BoundingBox) |  | Bounding box around the detected product. |
| row | [uint32](#uint32) |  | Row of product within shelf. |





 

 

 

 



## Scalar Value Types

| .proto Type | Notes | C++ Type | Java Type | Python Type |
| ----------- | ----- | -------- | --------- | ----------- |
| <a name="double" /> double |  | double | double | float |
| <a name="float" /> float |  | float | float | float |
| <a name="int32" /> int32 | Uses variable-length encoding. Inefficient for encoding negative numbers – if your field is likely to have negative values, use sint32 instead. | int32 | int | int |
| <a name="int64" /> int64 | Uses variable-length encoding. Inefficient for encoding negative numbers – if your field is likely to have negative values, use sint64 instead. | int64 | long | int/long |
| <a name="uint32" /> uint32 | Uses variable-length encoding. | uint32 | int | int/long |
| <a name="uint64" /> uint64 | Uses variable-length encoding. | uint64 | long | int/long |
| <a name="sint32" /> sint32 | Uses variable-length encoding. Signed int value. These more efficiently encode negative numbers than regular int32s. | int32 | int | int |
| <a name="sint64" /> sint64 | Uses variable-length encoding. Signed int value. These more efficiently encode negative numbers than regular int64s. | int64 | long | int/long |
| <a name="fixed32" /> fixed32 | Always four bytes. More efficient than uint32 if values are often greater than 2^28. | uint32 | int | int |
| <a name="fixed64" /> fixed64 | Always eight bytes. More efficient than uint64 if values are often greater than 2^56. | uint64 | long | int/long |
| <a name="sfixed32" /> sfixed32 | Always four bytes. | int32 | int | int |
| <a name="sfixed64" /> sfixed64 | Always eight bytes. | int64 | long | int/long |
| <a name="bool" /> bool |  | bool | boolean | boolean |
| <a name="string" /> string | A string must always contain UTF-8 encoded or 7-bit ASCII text. | string | String | str/unicode |
| <a name="bytes" /> bytes | May contain any arbitrary sequence of bytes. | string | ByteString | str |

