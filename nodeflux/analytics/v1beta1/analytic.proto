// Copyright 2018 PT Nodeflux Teknologi Indonesia.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

syntax = "proto3";

package nodeflux.analytics.v1beta1;

import "google/protobuf/any.proto";

option cc_enable_arenas = true;
option go_package = "gitlab.com/nodefluxio/nodefluxapis/analytics/v1beta1;analytics";
option java_multiple_files = true;
option java_outer_classname = "AnalyticProto";
option java_package = "io.nodeflux.analytics.v1beta1";

// The type of image or video analytic to be performed in
// the Nodeflux FIRE API.
message Analytic {
  // Type of Nodeflux FIRE APIs.
  enum Type {
    // Unspecified feature type.
    TYPE_UNSPECIFIED = 0;

    // Reserved for types not defined in the public FIRE API.
    // Used for custom analytics or prototypes.
    EXTENSION = 1;

    // Run face detection.
    FACE_DETECTION = 2;

    // Run face enrollment.
    FACE_ENROLLMENT = 3;

    // Run face recognition.
    FACE_RECOGNITION = 4;

    // Run face demography.
    FACE_DEMOGRAPHY = 5;

    // Run vehicle detection.
    VEHICLE_DETECTION = 6;

    // Run license plate recognition
    LICENSE_PLATE_RECOGNITION = 7;

    // Run 1:1 face match.
    FACE_MATCH = 8;

    // Run product detection.
    PRODUCT_DETECTION = 9;

    // Run shelf availability.
    ON_SHELF_AVAILABILITY = 10;

    // Run share on shelf.
    SHARE_ON_SHELF = 11;

    // Run shelf standard.
    SHELF_STANDARD = 12;

    // Run face pose.
    FACE_POSE = 13;

    // Run person detection.
    PERSON_DETECTION = 14;

    // Run face mask detection.
    FACE_MASK = 15;
  }

  // The analytic type to be performed.
  Type type = 1;

  // Extension name. Only valid if type is EXTENSION.
  string extension = 2;

  // Analytic specific options.
  google.protobuf.Any options = 3;
}
