.PHONY: all cpp python go docs lint check-googleapis clean

# Executables
PROTOC = protoc
PYTHON_PROTOC = python3 -m grpc_tools.protoc
GRPC_CPP_PLUGIN = grpc_cpp_plugin
GRPC_CPP_PLUGIN_PATH ?= `which $(GRPC_CPP_PLUGIN)`

# Paths
PWD = $(shell pwd)
PROTOS = $(shell find . -name "*.proto")
GOOGLEAPIS = ${GOOGLEAPIS_PATH}

# Output directories
CPP_OUT_DIR ?= cpp
PYTHON_OUT_DIR ?= python
GO_OUT_DIR ?= go
DOCS_OUT_DIR = docs

all: cpp python go docs lint

cpp: check-googleapis $(PROTOS)
	@echo "compiling protobuf and grpc for c++..."
	mkdir -p $(CPP_OUT_DIR)

	$(PROTOC) -I $(GOOGLEAPIS) -I $(PWD) --cpp_out=$(CPP_OUT_DIR) --grpc_out=$(CPP_OUT_DIR) --plugin=protoc-gen-grpc=$(GRPC_CPP_PLUGIN_PATH) $(filter-out $<,$^)

python: check-googleapis $(PROTOS)
	@echo "compiling protobuf and grpc for python..."
	mkdir -p $(PYTHON_OUT_DIR)

	$(PYTHON_PROTOC) -I $(GOOGLEAPIS) -I $(PWD) --python_out=$(PYTHON_OUT_DIR) --grpc_python_out=$(PYTHON_OUT_DIR) --mypy_out=$(PYTHON_OUT_DIR) $(filter-out $<,$^)

go: check-googleapis $(PROTOS)
	@echo "compiling protobuf and grpc for go..."
	mkdir -p $(GO_OUT_DIR)

	$(PROTOC) -I $(GOOGLEAPIS) -I $(PWD) --go_out=$(GO_OUT_DIR) $(filter nodeflux/analytics/v1beta1%,$^)
	$(PROTOC) -I $(GOOGLEAPIS) -I $(PWD) --go_out=plugins=grpc:$(GO_OUT_DIR) --grpc-gateway_out=allow_delete_body=true:$(GO_OUT_DIR) --swagger_out=allow_delete_body=true:$(GO_OUT_DIR) $(filter nodeflux/api/v1beta1%,$^)
	$(PROTOC) -I $(GOOGLEAPIS) -I $(PWD) --go_out=$(GO_OUT_DIR) $(filter nodeflux/types/v1beta1%,$^)
	$(PROTOC) -I $(GOOGLEAPIS) -I $(PWD) --go_out=$(GO_OUT_DIR) $(filter nodeflux/events/v1beta1%,$^)

	cp go.mod $(GO_OUT_DIR)/gitlab.com/nodefluxio/nodefluxapis

docs: check-googleapis $(PROTOS)
	@echo "compiling protobuf and grpc documentations..."
	mkdir -p $(DOCS_OUT_DIR)

	$(PROTOC) -I $(GOOGLEAPIS) -I $(PWD) --doc_out=$(DOCS_OUT_DIR) --doc_opt=markdown,analytics.md $(filter nodeflux/analytics/v1beta1%,$^)
	$(PROTOC) -I $(GOOGLEAPIS) -I $(PWD) --doc_out=$(DOCS_OUT_DIR) --doc_opt=markdown,api.md $(filter nodeflux/api/v1beta1%,$^)
	$(PROTOC) -I $(GOOGLEAPIS) -I $(PWD) --doc_out=$(DOCS_OUT_DIR) --doc_opt=markdown,types.md $(filter nodeflux/types/v1beta1%,$^)

lint: check-googleapis $(PROTOS)
	@echo "running linter..."

	$(PROTOC) -I $(GOOGLEAPIS) -I $(PWD) --lint_out=. $(filter-out $<,$^)

check-googleapis:
ifndef GOOGLEAPIS
	$(error GOOGLEAPIS_PATH environment variable is not set.)
endif

clean:
	rm -rf $(CPP_OUT_DIR)
	rm -rf $(PYTHON_OUT_DIR)
	rm -rf $(GO_OUT_DIR)
